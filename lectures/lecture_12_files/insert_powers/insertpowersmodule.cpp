#include <Python.h>

#include <iostream>
#include <sstream>
#include <string>

static PyObject *
insert_powers1(PyObject *self, PyObject *args)
{
   PyObject *numbers;
   int n;

   if (!PyArg_ParseTuple(args, "Oi", &numbers, &n)) {
      return NULL;
   }

   PyObject *powers = Py_BuildValue("(iii)", n, n*n, n*n*n);

   // Equivalent to Python: numbers[n] = powers
   if (PySequence_SetItem(numbers, n, powers) < 0) {
      return NULL;
   }

   return powers;
}

static PyObject *
insert_powers2(PyObject *self, PyObject *args)
{
   PyObject *numbers;
   int n;

   if (!PyArg_ParseTuple(args, "Oi", &numbers, &n)) {
      return NULL;
   }

   PyObject *powers = Py_BuildValue("(iii)", n, n*n, n*n*n);

   if (PySequence_SetItem(numbers, n, powers) < 0) {
      // Because we won't return powers, we have to discard it.
      Py_DECREF(powers);
      return NULL;
   }

   return powers;
}

static PyMethodDef
module_functions[] = {
   { "insert_powers1", insert_powers1, METH_VARARGS, "Insert a tuple of powers-of-n at index n" },
   { "insert_powers2", insert_powers2, METH_VARARGS, "Insert a tuple of powers-of-n at index n" },
   { NULL }
};

static struct PyModuleDef insert_powers_module =
{
    PyModuleDef_HEAD_INIT,
    "insert_powers",     /* name of module */
    "",          /* module documentation, may be NULL */
    -1,          /* size of per-interpreter state of the module, or -1 if the module keeps state in global variables. */
    module_functions
};

PyMODINIT_FUNC PyInit_insert_powers(void)
{
    return PyModule_Create(&insert_powers_module);
}
