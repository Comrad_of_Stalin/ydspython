#include <Python.h>

#include <iostream>
#include <sstream>
#include <string>

static PyObject *
string_peek(PyObject *self, PyObject *args)
{
   const char *pstr;
   int indx;

   if (!PyArg_ParseTuple(args, "si:string_peek", &pstr, &indx)) {
      return NULL;
   }

   int char_value = pstr[indx];

   return Py_BuildValue("i", char_value);
}

static PyObject *
string_peek2(PyObject *self, PyObject *args)
{
   const char *pstr;
   int indx;

   if (!PyArg_ParseTuple(args, "si:string_peek", &pstr, &indx)) {
      return NULL;
   }

   if (indx < 0 || indx >= strlen(pstr)) {
      // Bad index value, raise an exception.
      PyErr_SetString(PyExc_IndexError, "peek index out of range");
      return NULL;
   }

   int char_value = pstr[indx];

   return Py_BuildValue("i", char_value);
}

static PyMethodDef
module_functions[] = {
   { "string_peek", string_peek, METH_VARARGS, "Pick a character from a string." },
   { "string_peek2", string_peek2, METH_VARARGS, "Safely pick a character from a string." },
   { NULL }
};

static struct PyModuleDef string_peek_module =
{
    PyModuleDef_HEAD_INIT,
    "string_peek",     /* name of module */
    "",          /* module documentation, may be NULL */
    -1,          /* size of per-interpreter state of the module, or -1 if the module keeps state in global variables. */
    module_functions
};

PyMODINIT_FUNC PyInit_string_peek(void)
{
    return PyModule_Create(&string_peek_module);
}
