import ref_bug

import sys

class TestClass:
	def __str__(self):
		print("hello");

	def __del__(self):
		sys.stderr.write("I'm deleting\n")

class EvilDeleter:
	def __del__(self):
		del l[0]

if __name__ == '__main__':
	l = [TestClass()]

	l.append(EvilDeleter())

	mode = sys.argv[1]

	if mode == 'bug':
		ref_bug.ref_bug(l)
	else:
		ref_bug.ref_without_bug(l)
