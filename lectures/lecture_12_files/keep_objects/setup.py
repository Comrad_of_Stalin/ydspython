from distutils.core import setup, Extension

module1 = Extension('keep_objects', sources = ['keep_objects.cpp'], extra_compile_args = ['-std=c++11'])

setup (name = 'PackageName',
       version = '1.0',
       description = 'This is a demo package',
       ext_modules = [module1],
)
