def check_for_pos(i, s, odd):
    if not odd:
        l_ = i
        r = i+1
    else:
        l_ = i - 1
        r = i + 1
    ln = len(s)
    while l_ >= 0 and r < ln:
        if s[l_] != s[r]:
            return False, -1
        else:
            l_ -= 1
            r += 1
    return True, ln - (r - l_ - 1)


if __name__ == '__main__':
    s = input().strip()
    cur = len(s)
    for i in range(len(s)):
        for odd in [True, False]:
            can, num = check_for_pos(i, s, odd)
            if can:
                cur = min(cur, num)
    print(cur)
