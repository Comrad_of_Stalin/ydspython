import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

from test_utils import check_output


SCRIPT_FILENAME = './palindromes.py'


def test_example1():
    check_output(
        SCRIPT_FILENAME,

        # stdin
        'AwI\n',

        # stdout
        '2\n'
    )


def test_example2():
    check_output(
        SCRIPT_FILENAME,

        # stdin
        'zCssCzYzCssC\n',

        # stdout
        '1\n'
    )
