class LifeEmulation:
    def __init__(self, start_state, maximum_value):
        self._current_state = start_state
        self.maximum_value = maximum_value
        self.n = len(self._current_state)
        self.m = len(self._current_state[0])

    def __str__(self):
        return '\n'.join(
            [
                ' '.join([str(x) for x in line])
                for line in self._current_state
            ]
        )

    def get_neighbours_indexes(self, i, j):
        res = []
        for r in range(i - 1, i + 2):
            for c in range(j - 1, j + 2):
                if 0 <= r < self.n and 0 <= c < self.m and (r, c) != (i, j):
                    res.append((r, c))
        return res

    def get_neighbours_count_with_positive_value(self, i, j):
        cells = self.get_neighbours_indexes(i, j)
        res = 0
        for i, j in cells:
            if self._current_state[i][j] > 0:
                res += 1
        return res

    def get_next_state(self, i, j):
        new_state = self._current_state[i][j]
        neigh_boors = self.get_neighbours_count_with_positive_value(i, j)
        if self._current_state[i][j] == 0 and neigh_boors == 3:
            new_state = 1
        elif self._current_state[i][j] > 0 and neigh_boors in [2, 3]:
            new_state = self._current_state[i][j] + 1
        elif self._current_state[i][j] > 0 and neigh_boors in [1, 4, 5]:
            new_state = 0
        elif self._current_state[i][j] > 0 and neigh_boors in [0, 6, 8]:
            new_state = self._current_state[i][j] - 1
        if new_state > self.maximum_value:
            new_state = self.maximum_value
        return new_state

    def iterate(self):
        new_state = [
            [self.get_next_state(i, j) for j in range(self.m)]
            for i in range(self.n)
        ]

        self._current_state = new_state

    @property
    def current_state(self):
        return self._current_state
