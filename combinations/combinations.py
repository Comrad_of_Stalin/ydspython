from itertools import combinations


if __name__ == '__main__':
    words = sorted(set([x.lower() for x in input().split() if x]))
    for i in range(2, len(words)+1, 2):
        for comb in combinations(words, i):
            print(' '.join(comb))

