import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

from test_utils import check_output


SCRIPT_FILENAME = './combinations.py'


def test_example1():
    check_output(
        SCRIPT_FILENAME,

        # stdin
        'World Hello hello likeable first\n',

        # stdout
        'first hello\n'
        'first likeable\n'
        'first world\n'
        'hello likeable\n'
        'hello world\n'
        'likeable world\n'
        'first hello likeable world\n'
    )


def test_example2():
    check_output(
        SCRIPT_FILENAME,

        # stdin
        'bushwhack cool\n',

        # stdout
        'bushwhack cool\n'
    )


def test_example3():
    check_output(
        SCRIPT_FILENAME,

        # stdin
        'bushwhack bushwhack bushwhacs\n',

        # stdout
        'bushwhack bushwhacs\n'
    )
