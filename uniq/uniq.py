import argparse
from collections import OrderedDict
import sys


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--count', default=False, action='store_true')
    parser.add_argument('-d', '--repeat', default=False, action='store_true')
    parser.add_argument('-i', '--ignore-case',
                        default=False, action='store_true')
    parser.add_argument('-u', '--unique', default=False, action='store_true')

    lines = sys.stdin.read().split('\n')

    args = parser.parse_args(lines[0].strip().split())
    n = int(lines[1].strip())
    d = OrderedDict()

    for line in lines[2:]:
        if args.ignore_case:
            keyline = line.lower()
        else:
            keyline = line
        if keyline == '':
            continue
        if keyline in d:
            d[keyline][1] += 1
        else:
            d[keyline] = [line, 1]
    result = []
    for k, v in d.items():
        if args.unique:
            if v[1] > 1:
                continue
            elif args.count:
                print('{} {}'.format(1, v[0]))
            else:
                print(v[0])
        elif args.repeat:
            if v[1] <= 1:
                continue
            elif args.count:
                print('{} {}'.format(v[1], v[0]))
            else:
                print(v[0])
        else:
            if args.count:
                print('{} {}'.format(v[1], v[0]))
            else:
                print(v[0])
