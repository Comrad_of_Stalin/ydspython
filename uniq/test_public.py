import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

from test_utils import check_output


SCRIPT_FILENAME = './uniq.py'


def test_example1():
    check_output(
        SCRIPT_FILENAME,

        # stdin
        '-i --unique\n'
        '16\n'
        'hRcWbYrIDUzKoabTvfyNELjQOBbmQtpkELGuMwMuhSkhjHOUsZEAnANHzMxiKOXXsPSMPPv\n'
        'HRcwBYRiduzkoaBTvFyNEljQoBbmQTPkeLGuMwMuhSKhJhoUSzeaNANHZmxIKOXXspsMppV\n'
        'GRiVKe\n'
        'jtMoGnmBauAtDGoIOPzGdnDwfsfLJslVHsxQMgKUEFouvModghDOmptPSJWCrbiaoXaFrAennsnJuKHeIQTXB\n'
        'mkbDzjQfADEMnWKhEYPGNHSvcTiWjkmSScNuYiBBvdhxLrtbNIltTkhK\n'
        'NMqq\n'
        'nmQq\n'
        'jTCbFCrckJXtDwWNQrrooJBI\n'
        'kbKbkvuqxrrcQOJNyxWwmUzCkdVjGAuUgtrVvwrsJfhrHzLWuHidKmLnStqRhZgHdmtfgSdZkosXpxatuiHmNWVwtj\n'
        'hSgojOuLuVZjjdPrSpXBvcndUCfRhmp\n'
        'HsgOJouLuVzJjDPRSpXbVCnDUcFRHMP\n'
        'umfWDFqXAiHwGT\n'
        'BvEtICVHMUMWmzizuliJQBxbSujSRaDzEGGdoqwIsXpyLUrWSTb\n'
        'oRRSKYjlbrqHjthd\n'
        'QnrjrYPCEeSyiNPjWsoEoIDMlrpZSecGCYJcdRbRpHtRGhWNMkJXhcvAmCotbxeaQWksPsAPRaTCYXfOxDnHSJXFSKtT\n'
        'sNczMaxQnbLqWgbZTkZcEOOtvtEfsMpXixAefrsEGSpsHXuzNtRXPtVFckTolRMLgBCwcusALkLZNerIQhb\n',

        # stdout
        'GRiVKe\n'
        'jtMoGnmBauAtDGoIOPzGdnDwfsfLJslVHsxQMgKUEFouvModghDOmptPSJWCrbiaoXaFrAennsnJuKHeIQTXB\n'
        'mkbDzjQfADEMnWKhEYPGNHSvcTiWjkmSScNuYiBBvdhxLrtbNIltTkhK\n'
        'jTCbFCrckJXtDwWNQrrooJBI\n'
        'kbKbkvuqxrrcQOJNyxWwmUzCkdVjGAuUgtrVvwrsJfhrHzLWuHidKmLnStqRhZgHdmtfgSdZkosXpxatuiHmNWVwtj\n'
        'umfWDFqXAiHwGT\n'
        'BvEtICVHMUMWmzizuliJQBxbSujSRaDzEGGdoqwIsXpyLUrWSTb\n'
        'oRRSKYjlbrqHjthd\n'
        'QnrjrYPCEeSyiNPjWsoEoIDMlrpZSecGCYJcdRbRpHtRGhWNMkJXhcvAmCotbxeaQWksPsAPRaTCYXfOxDnHSJXFSKtT\n'
        'sNczMaxQnbLqWgbZTkZcEOOtvtEfsMpXixAefrsEGSpsHXuzNtRXPtVFckTolRMLgBCwcusALkLZNerIQhb\n',
    )


# def test_example2():
#     check_output(
#         SCRIPT_FILENAME,
#
#         # stdin
#         '--count\n'
#         '11\n'
#         'MtdiFyFQTOZOPpiSIcbFyxoPagWKzihmFRiVcVJLsXJZDJiPtZeOGlxlCy\n'
#         'AjRMbziHSCJEwmVFONiVRiXqWjyCNZghQVvqkFTnoqyQOrhfzl\n'
#         'rRNXvFYimSAuyksleBlesHXFVlJMTSIcEHzihqOplc\n'
#         'gPddRtkPVYuSUWnZwMGqptRXOuBAzzlsKPsHjamRXdQZZNhRLqPUqABrlMccmvRyjgCCRFnJaMofKsPHLqIdKiXVn\n'
#         'PGKGrvkIAcBANqQLYcYyxRMwzwfEweGmDPBJsjUreQtpdniCZmIAEArjGhwmjuReEwMswPoqpbFSRSLJEmXRnBv\n'
#         'rAXGnfM\n'
#         'rAXGnfM\n'
#         'rAXGnfM\n'
#         'ACGqrXDpruLGLkVwKSJVpvEJLXvhYldyZtWZUDPeCHgNzBNIoRWOKJBhcCdCtWroDWCMhMCJfapn\n'
#         'trnCxxJDUcoqRBybwUqXJkKsNsAXqJ\n'
#         'cRbzHxcqUxoOEVHbejDXZCKOIMbbriEAaYVHVNk\n',
#
#         # stdout
#         '1 MtdiFyFQTOZOPpiSIcbFyxoPagWKzihmFRiVcVJLsXJZDJiPtZeOGlxlCy\n'
#         '1 AjRMbziHSCJEwmVFONiVRiXqWjyCNZghQVvqkFTnoqyQOrhfzl\n'
#         '1 rRNXvFYimSAuyksleBlesHXFVlJMTSIcEHzihqOplc\n'
#         '1 gPddRtkPVYuSUWnZwMGqptRXOuBAzzlsKPsHjamRXdQZZNhRLqPUqABrlMccmvRyjgCCRFnJaMofKsPHLqIdKiXVn\n'
#         '1 PGKGrvkIAcBANqQLYcYyxRMwzwfEweGmDPBJsjUreQtpdniCZmIAEArjGhwmjuReEwMswPoqpbFSRSLJEmXRnBv\n'
#         '3 rAXGnfM\n'
#         '1 ACGqrXDpruLGLkVwKSJVpvEJLXvhYldyZtWZUDPeCHgNzBNIoRWOKJBhcCdCtWroDWCMhMCJfapn\n'
#         '1 trnCxxJDUcoqRBybwUqXJkKsNsAXqJ\n'
#         '1 cRbzHxcqUxoOEVHbejDXZCKOIMbbriEAaYVHVNk\n'
#     )
