## Утилита uniq

### Условие
Нужно реализовать аналог консольной утилиты uniq. Эта утилита выводит только первую из идущих подряд повторяющихся строк. Программа должна уметь принимать ключи:
1. -c, –count, выводить число повторов в начале каждой строки;
2. -d, –repeat, выводить только повторяющиеся строки;
3. -i, –ignore-case, игнорировать регистр при сравнении;
4. -u, –unique, выводить только неповторяющиеся строки.

Обратите внимание, что ключей может быть несколько и всегда указан хотя бы один ключ. Гарантируется, что оба ключа -d, –repeat и -u, –unique не могут быть указаны одновременно.

### Формат ввода
На первой строке указаны ключи, разделенный через пробел. На второй строке задано число `n` (`1 <=n <= 10000`). Далее идут `n` строк, стоящие из строчных или прописных букв латинского алфавита. Длина каждой строки не превосходит `100` символов.

### Формат вывода
Вывести результат выполнения, аналогичный запуску uniq с заданными ключами.

### Пример
```bash
(venv) kolina93@kolina93-Latitude-E7440:~/shad/tasks/uniq$ cat input.txt 
-i --unique
16
hRcWbYrIDUzKoabTvfyNELjQOBbmQtpkELGuMwMuhSkhjHOUsZEAnANHzMxiKOXXsPSMPPv
HRcwBYRiduzkoaBTvFyNEljQoBbmQTPkeLGuMwMuhSKhJhoUSzeaNANHZmxIKOXXspsMppV
GRiVKe
jtMoGnmBauAtDGoIOPzGdnDwfsfLJslVHsxQMgKUEFouvModghDOmptPSJWCrbiaoXaFrAennsnJuKHeIQTXB
mkbDzjQfADEMnWKhEYPGNHSvcTiWjkmSScNuYiBBvdhxLrtbNIltTkhK
NMqq
nmQq
jTCbFCrckJXtDwWNQrrooJBI
kbKbkvuqxrrcQOJNyxWwmUzCkdVjGAuUgtrVvwrsJfhrHzLWuHidKmLnStqRhZgHdmtfgSdZkosXpxatuiHmNWVwtj
hSgojOuLuVZjjdPrSpXBvcndUCfRhmp
HsgOJouLuVzJjDPRSpXbVCnDUcFRHMP
umfWDFqXAiHwGT
BvEtICVHMUMWmzizuliJQBxbSujSRaDzEGGdoqwIsXpyLUrWSTb
oRRSKYjlbrqHjthd
QnrjrYPCEeSyiNPjWsoEoIDMlrpZSecGCYJcdRbRpHtRGhWNMkJXhcvAmCotbxeaQWksPsAPRaTCYXfOxDnHSJXFSKtT
sNczMaxQnbLqWgbZTkZcEOOtvtEfsMpXixAefrsEGSpsHXuzNtRXPtVFckTolRMLgBCwcusALkLZNerIQhb
(venv) kolina93@kolina93-Latitude-E7440:~/shad/tasks/uniq$ python uniq.py < input.txt 
GRiVKe
jtMoGnmBauAtDGoIOPzGdnDwfsfLJslVHsxQMgKUEFouvModghDOmptPSJWCrbiaoXaFrAennsnJuKHeIQTXB
mkbDzjQfADEMnWKhEYPGNHSvcTiWjkmSScNuYiBBvdhxLrtbNIltTkhK
jTCbFCrckJXtDwWNQrrooJBI
kbKbkvuqxrrcQOJNyxWwmUzCkdVjGAuUgtrVvwrsJfhrHzLWuHidKmLnStqRhZgHdmtfgSdZkosXpxatuiHmNWVwtj
umfWDFqXAiHwGT
BvEtICVHMUMWmzizuliJQBxbSujSRaDzEGGdoqwIsXpyLUrWSTb
oRRSKYjlbrqHjthd
QnrjrYPCEeSyiNPjWsoEoIDMlrpZSecGCYJcdRbRpHtRGhWNMkJXhcvAmCotbxeaQWksPsAPRaTCYXfOxDnHSJXFSKtT
sNczMaxQnbLqWgbZTkZcEOOtvtEfsMpXixAefrsEGSpsHXuzNtRXPtVFckTolRMLgBCwcusALkLZNerIQhb
(venv) kolina93@kolina93-Latitude-E7440:~/shad/tasks/uniq$ 
```
