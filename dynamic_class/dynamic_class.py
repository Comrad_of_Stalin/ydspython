def attributes_finder(oracle, attribute_choices, attributes_count):
    def make_query(_oracle, pos, attr_array):
        return getattr(_oracle, attr_array[pos])

    def find_next(_oracle, last_found_pos, total, attr_array, already_taken):
        pow2 = 0
        while (1 << pow2) < total - last_found_pos - 1:
            pow2 += 1

        pow2 -= 1
        cur = last_found_pos + 1
        for i in range(pow2, -1, -1):
            if cur + (1 << i) < total:
                if make_query(_oracle, cur + (1 << i), attr_array) == already_taken:
                    cur += 1 << i
        return cur

    attribute_choices.sort()
    bit_mask = [0] * len(attribute_choices)
    taken = 0
    max_pos = -1
    while taken < attributes_count:
        nxt_pos = find_next(
            oracle, max_pos,
            len(attribute_choices),
            attribute_choices,
            taken
        )
        bit_mask[nxt_pos] = 1
        max_pos = nxt_pos
        taken += 1

    class Fake:
        pass

    ans = Fake()

    for i in range(len(attribute_choices)):
        if bit_mask[i] == 1:
            setattr(ans, attribute_choices[i], 1)

    return ans
