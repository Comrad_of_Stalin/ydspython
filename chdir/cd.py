

def chdir(current_path, command_path):
    arr = [x for x in current_path.split('/') if x]
    if command_path.startswith('/'):
        arr = []
    arr += [x for x in command_path.split('/') if x]
    cur_dir = []
    for x in arr:
        if x == '..':
            if cur_dir:
                cur_dir.pop()
        elif x == '.':
            continue
        else:
            cur_dir.append(x)
    return '/' + '/'.join(cur_dir)
