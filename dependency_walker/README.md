## Dependency walker

### Условие
Одной из важных частей поисковых систем является так называемый робот - система, задачей которой является обход
интернет-страниц с целью иметь внутри как можно более точное состояние текущего интернета на данный момент. Перед
роботом стоит много задачей: как находить страницы, которые нужно обойти, как выбрать страницы, которые нужно обойти,
с какой частотой их обходить и много других. Одним из механизмов обнаружения новых ссылок является обнаружение ссылок
в известных страницах, т.е. построение ссылочного графа. В этой задаче мы рассмотрим упрощенный вариант такой задачи.

Есть некоторый сервер, поднятый на локальном интерфейсе (`localhost`), чтобы избежать сетевых проблем. Адрес сервера
задан параметром `server_url`. Сервер умеет отвечать по адресам вида `{server_url}/{entity_id}`, по которому
возвращается список зависимостей `entity_id` (аналогия ссылок на веб-странице). Ваша цель, начиная с некоторого
стартового `start_id`, получить все достижимые `entity_id`, которые можно обойти начиная с этой страницы `start_id`,
проходя рекурсивно по полученным зависимостям. При этом известно, что сервер может одновременно может обрабатывать до
`server_throughput` запросов и может на некоторые запросы отвечать с задержками до 4 секунд. Вам нужно построить как
можно более эффективный обход этого сервера и уложиться в 25 секунд в публичных тестах и 60 секунд в приватных тестах.

### Пример
Для публичных тестов нужно поставить фреймворк `cherrypy` через `pip`.

Запустить сервер можно вот такой командой

```bash
(venv) kolina93@kolina93-Latitude-E7440:~/shad/tasks/dependency_walker$ python simple_server.py
[03/Dec/2018:00:55:40] ENGINE Listening for SIGTERM.
[03/Dec/2018:00:55:40] ENGINE Listening for SIGHUP.
[03/Dec/2018:00:55:40] ENGINE Listening for SIGUSR1.
[03/Dec/2018:00:55:40] ENGINE Bus STARTING
CherryPy Checker:
The Application mounted at '' has an empty config.

[03/Dec/2018:00:55:40] ENGINE Started monitor thread 'Autoreloader'.
[03/Dec/2018:00:55:40] ENGINE Serving on http://127.0.0.1:8080
[03/Dec/2018:00:55:40] ENGINE Bus STARTED
```

```python
In [1]: from dependency_walker import walk

In [2]: %time walk('http://localhost:8080', 'MkcVESOVmz', 4)
CPU times: user 92 ms, sys: 12 ms, total: 104 ms
Wall time: 16.1 s
Out[2]:
{'FTmSrAiTaV',
 'FWbVIXSYRy',
 'LkYldmVSaf',
 'MkcVESOVmz',
 'NxINrUjdpV',
 'SCDjuMMEPp',
 'TlvNMAwKxV',
 'VERmYMpXnK',
 'aLhIJRWzcB',
 'gjsvHbjaUS',
 'hkmJtxwHxP',
 'kLWadKxEXB',
 'lAZWvrlWnY',
 'lJKqmGZhrn',
 'pKhOyKqlYM',
 'tqBAKAWZTb'}

In [3]:
```

### Пример запуска тестов
Запускаются тесты из файла ```test_public.py```.
```bash
(venv) kolina93@kolina93-Latitude-E7440:~/shad/tasks/dependency_walker$ pytest test_public.py
```
