class ExtendedDict(dict):
    def __init__(self, *args, **kwargs):
        dict.__init__(self, *args, **kwargs)

    def __setattr__(self, key, value):
        self.__setitem__(key, value)

    def __setitem__(self, key, value):
        if not isinstance(key, str):
            raise ValueError("Foo")
        dict.__setitem__(self, key, value)

    def __getattr__(self, item):
        return self.__getitem__(item)

    def __getitem__(self, item):
        if isinstance(item, slice):
            return sorted(self.items())[item]
        if not isinstance(item, str):
            raise ValueError("Foo")
        return dict.__getitem__(self, item)

    def __delattr__(self, item):
        self.__delitem__(item)

    def __delitem__(self, key):
        if not isinstance(key, str):
            raise ValueError("Foo")
        dict.__delitem__(self, key)
