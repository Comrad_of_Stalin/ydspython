from .property_maker import PropertyMaker


# noinspection SpellCheckingInspection
def test_example(capsys):
    # noinspection PyAttributeOutsideInit
    class C(metaclass=PropertyMaker):
        def get_x(self):
            print("get x")
            return self._x

        def set_x(self, value):
            print("set x")
            self._x = value

    c = C()
    c.x = 1
    assert c.x == 1
    stdout, _ = capsys.readouterr()
    assert stdout == "set x\nget x\n"
