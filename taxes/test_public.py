import numpy

from .taxes import compute_progressive_taxes


def test_taxes():
    assert numpy.isclose(compute_progressive_taxes(numpy.array([100, 200, 800, 200])), 183.0)
    assert numpy.isclose(compute_progressive_taxes(numpy.array([100, 200, 700, 200])), 156.0)
    assert numpy.isclose(compute_progressive_taxes(numpy.array([100, 200, 700, 200, 200])), 196.0)
