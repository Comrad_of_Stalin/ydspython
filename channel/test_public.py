from threading import Thread

from pytest import raises

from .channel import Channel, ChannelClosedException


class ThreadJoiner:
    def __init__(self):
        self._threads = []

    def add_thread(self, thread):
        # Must do do this to prevent program's hang on exit in case of thread's hang
        thread.daemon = True
        thread.start()
        self._threads.append(thread)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        exception = None
        for thread in self._threads:
            try:
                thread.join(timeout=5)
                if thread.is_alive():
                    raise RuntimeError('One of threads did not finished in 5 seconds, probably it hanged up.')
            except BaseException as e:
                exception = e

        if exception is not None:
            raise exception


class ExceptionPreserveThread(Thread):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._exception = None

    def run(self):
        try:
            super().run()
        except BaseException as e:
            self._exception = e

    def join(self, timeout=None):
        super().join(timeout=timeout)
        if self._exception:
            raise self._exception


def test_channel_simple():
    channel = Channel()

    def _sender():
        channel.send(1)

    def _receiver():
        value = channel.recv()
        assert value == 1

    with ThreadJoiner() as joiner:
        joiner.add_thread(ExceptionPreserveThread(target=_sender))
        joiner.add_thread(ExceptionPreserveThread(target=_receiver))

    channel.close()

    with raises(ChannelClosedException):
        channel.send(1)

    with raises(ChannelClosedException):
        channel.recv()
