import typing


class ChannelClosedException(Exception):
    pass


class Channel:
    def send(self, value: typing.Any) -> None:
        pass

    def recv(self):
        pass

    def close(self):
        pass
