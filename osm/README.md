## OpenStreetMap

### Условие

[OpenStreeMap](https://www.openstreetmap.org) - open-source проект, целью которого является создать подробную
online-карту мира силами сообщества. Для этого используются данные персональных GPS-трекеров, аэрофотографии,
видеозаписи, спутниковые снимки и панорамы улиц, предоставленные некоторыми компаниями, а также знания человека,
рисующего карту. В этой задаче вам предстоит научиться доставать информацию о местах с их
[поискового движка](https://nominatim.openstreetmap.org). Если там ввести в поиск описание некоторого места
(например, "Минск"), то покажется следующая информация:

![OSM search](osm_search.png)

Далее можно кликнуть на один из результатов и на клику по "Details" будет такая информация:

![OSM result](osm_result.png)

Нужно написать функцию `get_place_data`, которая принимает один параметр `place` - название местности (например,
"Минск") и возвращает следующую информацию для первых 5 первых результатов поиска `place` (если результатов меньше 5,
то возвращает информацию для всех результатов) в виде `dict`:

* `name` - имя результата и `type` - тип результата (это будет "Минск, Беларусь" и "City" для первого результата ниже):

![Name and type](name_and_type.png)

* `lat`, `lon` - координаты результата в виде `float`, доступные по переходу на кнопку "Details" (это будет
`53.902334` и `27.5618791` для примера ниже)

![Coordinates](coordinates.png)

* `extra_tags`, `address_tags`: содержимое полей `Extra Tags` и `Address Tags` в виде `dict`, для примера ниже должно
быть:

```python
{
    'capital': 'yes',
    'name:prefix': 'город',
    'place': 'city',
    'population': '1982444',
    'website': 'http://minsk.gov.by/',
    'wikidata': 'Q2280',
    'wikipedia': 'be:Горад Мінск',
    'wikipedia:be': 'Мінск',
    'wikipedia:en': 'Minsk',
    'wikipedia:pl': 'Mińsk',
}

{'continent': 'Europe', 'country': 'BY'}
```

![Tags](tags.png)

Полные примеры ответа можно посмотреть в тестовых файлах для публичных тестов.

### Пример
```python
In [1]: from osm import get_place_data

In [2]: get_place_data('минск')
Out[2]:
[{'name': 'Минск, Беларусь',
  'type': 'City',
  'extra_tags': {'capital': 'yes',
   'name:prefix': 'город',
   'place': 'city',
   'population': '1982444',
   'website': 'http://minsk.gov.by/',
   'wikidata': 'Q2280',
   'wikipedia': 'be:Горад Мінск',
   'wikipedia:be': 'Мінск',
   'wikipedia:en': 'Minsk',
   'wikipedia:pl': 'Mińsk'},
  'address_tags': {'continent': 'Europe', 'country': 'BY'},
  'admin_level': 4,
  'lat': 53.902334,
  'lon': 27.5618791},
 {'name': 'Минск, Беларусь',
  'type': 'State',
  'extra_tags': {},
  'address_tags': {},
  'admin_level': 4,
  'lat': 53.8791852,
  'lon': 27.8995566219764},
 {'name': 'Minsk, Беларусь',
  'type': 'State',
  'extra_tags': {},
  'address_tags': {},
  'admin_level': 4,
  'lat': 53.8395964,
  'lon': 27.4711051089346},
 {'name': 'Minsk, кв. Модерно предградие, ж.к. Люлин 9, Люлин, Sofia City, София, 1360, Болгария',
  'type': 'Residential',
  'extra_tags': {},
  'address_tags': {},
  'admin_level': 15,
  'lat': 42.7227445,
  'lon': 23.2693328},
 {'name': 'Minsk, кв.Изток, кв. Мошино, Golemo Buchino, Pernik, Перникская область, 2304, Болгария',
  'type': 'Residential',
  'extra_tags': {},
  'address_tags': {},
  'admin_level': 15,
  'lat': 42.6086625,
  'lon': 23.0989321}]

In [3]:
```

### Пример запуска тестов
Запускаются тесты из файла ```test_public.py```.
```bash
(venv) kolina93@kolina93-Latitude-E7440:~/shad/tasks/osm$ pytest
```

### Примечание
**Для того, чтобы выдача совпадала с тем, что в тестах, вам нужно установить русский язык в настройках вашего браузера.**

Для запросов нужно использовать библиотеку `requests`, для парсинга нужно использовать библиотеки
`BeautifulSoup` или `lxml`. Для понимания того, какие именно запросы делаются в браузере, могут быть полезны
[Developer Tools](https://en.wikipedia.org/wiki/Web_development_tools), как показывалось на лекции про работу с вебом.
