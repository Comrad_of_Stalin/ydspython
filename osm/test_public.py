import pytest

from .osm import get_place_data

CASES = {
    'минск': [
        {
            'name': 'Минск, Беларусь',
            'type': 'City',
            'lat': 53.902334,
            'lon': 27.5618791,
            'extra_tags': {
                'capital': 'yes',
                'name:prefix': 'город',
                'place': 'city',
                'population': '1982444',
                'website': 'http://minsk.gov.by/',
                'wikidata': 'Q2280',
                'wikipedia': 'be:Горад Мінск',
                'wikipedia:be': 'Мінск',
                'wikipedia:en': 'Minsk',
                'wikipedia:pl': 'Mińsk',
            },
            'address_tags': {'continent': 'Europe', 'country': 'BY'},
            'admin_level': 4,
        },
        {
            'name': 'Минск, Беларусь',
            'type': 'State',
            'lat': 53.8791852,
            'lon': 27.8995566219764,
            'extra_tags': {},
            'address_tags': {},
            'admin_level': 4,
        },
        {
            'name': 'Minsk, Беларусь',
            'type': 'State',
            'lat': 53.8395964,
            'lon': 27.4711051089346,
            'extra_tags': {},
            'address_tags': {},
            'admin_level': 4,
        },
        {
            'name': 'Minsk, кв. Модерно предградие, ж.к. Люлин 9, Люлин, Sofia City, София, 1360, Болгария',
            'type': 'Residential',
            'lat': 42.7227445,
            'lon': 23.2693328,
            'extra_tags': {},
            'address_tags': {},
            'admin_level': 15,
        },
        {
            'name': 'Minsk, кв.Изток, кв. Мошино, Golemo Buchino, Pernik, Перникская область, 2304, Болгария',
            'type': 'Residential',
            'lat': 42.6086625,
            'lon': 23.0989321,
            'extra_tags': {},
            'address_tags': {},
            'admin_level': 15,
        },
    ],
    'добромысленский переулок, 4': [
        {
            'name': '4, Добромысленский переулок, Грушевка, Московский район, Минск, 220007, Беларусь',
            'type': 'Building',
            'lat': 53.8917074,
            'lon': 27.5387669018881,
            'extra_tags': {},
            'address_tags': {
                'city': 'Минск',
                'housenumber': '4',
                'postcode': '220007',
                'street': 'Добромысленский переулок',
            },
            'admin_level': 15,
        },
    ],
    'москва': [
        {
            "name": "Москва, Центральный административный округ, Москва, Центральный федеральный округ, РФ",
            "type": "City",
            "extra_tags": {
                "capital": "yes",
                "place": "city",
                "population": "12380664",
                "wikidata": "Q649",
                "wikipedia": "ru:Москва"
            },
            "address_tags": {
                "country": "RU",
                "region": "Москва"
            },
            "admin_level": 15,
            "lat": 55.7507178,
            "lon": 37.6176606,
        },
        {
            "name": "Москва, Центральный федеральный округ, РФ",
            "type": "State",
            "extra_tags": {
                "place": "state",
                "wikidata": "Q649",
                "wikipedia": "ru:Москва"
            },
            "address_tags": {
                "country": "RU",
                "region": "Москва"
            },
            "admin_level": 4,
            "lat": 55.4792046,
            "lon": 37.3273304,
        },
        {
            "name": "Москва, Центральный федеральный округ, 143059, РФ",
            "type": "River",
            "extra_tags": {
                "wikidata": "Q175117",
                "wikipedia": "ru:Москва (река)"
            },
            "address_tags": {},
            "admin_level": 15,
            "lat": 55.7098009,
            "lon": 37.0536908,
        },
        {
            "name": "Москва, сельское поселение Ашитковское, Воскресенский район, Московская область, "
                    "Центральный федеральный округ, 140230, РФ",
            "type": "River",
            "extra_tags": {
                "wikidata": "Q175117",
                "wikipedia": "ru:Москва (река)"
            },
            "address_tags": {},
            "admin_level": 15,
            "lat": 55.4033937,
            "lon": 38.5070172,
        },
        {
            "name": "Москва, Развилковское сельское поселение, Ленинский район, Московская область, "
                    "Центральный федеральный округ, 109649, РФ",
            "type": "River",
            "extra_tags": {
                "wikidata": "Q175117",
                "wikipedia": "ru:Москва (река)"
            },
            "address_tags": {},
            "admin_level": 15,
            "lat": 55.6095218,
            "lon": 37.8089581,
        },
    ],
}


@pytest.mark.parametrize('place,expected', CASES.items())
def test_places_search(place, expected):
    assert get_place_data(place) == expected
