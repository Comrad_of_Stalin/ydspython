from collections import defaultdict
import copy
from string import punctuation


def super_split(string, chars, ind=0):
    if ind == len(chars):
        return string
    char = chars[ind]
    string = ''.join(string.split(char))
    return super_split(string, chars, ind+1)


if __name__ == '__main__':
    chars = list(punctuation)
    n = int(input().strip())

    lines = [super_split(input(), chars) for i in range(n)]
    sentences = [[x.lower() for x in line.split() if x] for line in lines]

    word_sets = defaultdict(lambda: set())

    for ind, sent in enumerate(sentences):
        for word in sent:
            word_sets[word].add(ind)

    m = int(input().strip())

    for i in range(m):
        q = list(set([x.lower() for x in input().split() if x]))
        words = copy.deepcopy(word_sets[q[0]])
        for word in q[1:]:
            words &= word_sets[word]
        if len(words) == 0:
            print(-1)
        else:
            print(' '.join(str(i+1) for i in sorted(words)))
