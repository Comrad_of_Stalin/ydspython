import math


def merge_sort(iterable, list_store=None, return_combinations=True):
    if list_store is None:
        list_store = []
    elems = list(iterable)
    ln = len(elems)

    stage = int(math.log2(ln))
    while len(list_store) < stage:
        list_store.append([])
    res = []
    if ln == 1:
        if return_combinations:
            # raise Exception('HEEEEEEEEEEEEEEEEEEELP')
            # yield elems[0]
            raise StopIteration()
    else:
        if ln == 2:
            for i in [min(elems), max(elems)]:
                res.append(i)
                list_store[stage - 1].append(i)
        else:
            fir_seq = list(merge_sort(elems[:ln//2], list_store, False))
            sec_seq = list(merge_sort(elems[ln//2:], list_store, False))

            x, y = 0, 0
            seq_ln = ln//2
            while x != seq_ln and y != seq_ln:
                curx, cury = fir_seq[x], sec_seq[y]
                if curx < cury:
                    res.append(curx)
                    list_store[stage-1].append(curx)
                    x += 1
                else:
                    res.append(cury)
                    list_store[stage-1].append(cury)
                    y += 1

            for elx in fir_seq[x:]:
                res.append(elx)
                list_store[stage-1].append(elx)
            for ely in sec_seq[y:]:
                res.append(ely)
                list_store[stage-1].append(ely)

        if return_combinations:
            for i in list_store:
                yield i
        else:
            for i in res:
                yield i


if __name__ == '__main__':
    print(list(merge_sort([2, 1, 5, 8, 11, 12, 0, 1])))
