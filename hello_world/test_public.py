from .hello_world import get_hello_world


def test_hello_world():
    answer = get_hello_world()
    assert answer == "Hello, world!"
