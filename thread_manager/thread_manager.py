import threading
import typing


class ThreadManager:
    def start_thread(self) -> threading.Thread:
        raise NotImplementedError

    def stop_thread(self, thread_id: int) -> list:
        raise NotImplementedError

    def call_in_thread(self, thread_id: int, func: typing.Callable, *args, **kwargs) -> None:
        raise NotImplementedError
