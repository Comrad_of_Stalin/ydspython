import inspect


def annotated_func(args):
    for arg in args:
        if not (isinstance(arg, type) or any(isinstance(arg, t) for t in [
            TypedAny,
            TypedUnion,
            TypedList,
            TypedDict,
            TypedTuple,
            TypedSet,
            TypedCallable,
            TypedAnnotatedCallable
        ])):
            raise TypeError()

    def wrapper(func):
        arg_num = len(inspect.getargspec(func)[0])
        if arg_num != len(args):
            raise TypeError()

        def in_wrapper(*fargs):
            return func(*fargs)

        setattr(in_wrapper, '__types__', list(args))

        return in_wrapper

    return wrapper


class TypedAny:
    def __instancecheck__(self, instance):
        return True

    def __eq__(self, other):
        return True


class TypedUnion:
    def __init__(self, types_list):
        self.types_list = types_list

    def __instancecheck__(self, instance):
        return any(isinstance(instance, x) for x in self.types_list)

    def __eq__(self, other):
        return self.__dict__ == other.__dict__


class TypedList:
    def __init__(self, t):
        self.T = t

    def __instancecheck__(self, instance):
        if not isinstance(instance, list):
            return False
        return all(isinstance(x, self.T) for x in instance)

    def __eq__(self, other):
        return self.__dict__ == other.__dict__


class TypedTuple:
    def __init__(self, t):
        self.T = t

    def __instancecheck__(self, instance):
        if not isinstance(instance, tuple):
            return False
        return all(isinstance(x, self.T) for x in instance)

    def __eq__(self, other):
        return self.__dict__ == other.__dict__


class TypedDict:
    def __init__(self, k, v):
        self.K = k
        self.V = v

    def __instancecheck__(self, instance):
        if not isinstance(instance, dict):
            return False
        return all((isinstance(k, self.K) and isinstance(v, self.V)) for k, v in instance.items())

    def __eq__(self, other):
        return self.__dict__ == other.__dict__


class TypedSet:
    def __init__(self, t):
        self.T = t

    def __instancecheck__(self, instance):
        if not isinstance(instance, set):
            return False
        return all(isinstance(x, self.T) for x in instance)

    def __eq__(self, other):
        return self.__dict__ == other.__dict__


class TypedCallable:
    def __instancecheck__(self, instance):
        return callable(instance)

    def __eq__(self, other):
        return self.__dict__ == other.__dict__


class TypedAnnotatedCallable:
    def __init__(self, types_list):
        self.types_list = types_list

    def __instancecheck__(self, instance):
        if not callable(instance):
            return False
        if not hasattr(instance, '__types__'):
            return False
        if len(instance.__types__) != len(self.types_list):
            return False
        for i in range(len(instance.__types__)):
            if not(instance.__types__[i] == self.types_list[i]):
                return False

        return True

    def __eq__(self, other):
        return self.__dict__ == other.__dict__


def type_check(func):
    def wrapped(*args):
        args_copy = list(args)
        if len(args_copy) != len(func.__types__):
            raise TypeError()
        for i in range(len(func.__types__)):
            arg = args_copy[i]
            tp = func.__types__[i]
            if not isinstance(arg, tp):
                raise TypeError()
        return func(*args)
    return wrapped
