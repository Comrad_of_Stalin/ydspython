# noinspection PyShadowingBuiltins
from .super_super import super_super as super


class A:
    # noinspection PyMethodMayBeStatic
    def f(self):
        return 'A'

    @classmethod
    def cm(cls):
        return cls, 'A'

    # noinspection PyMethodMayBeStatic
    def a(self):
        return 'A'


class D(A):
    def f(self):
        return super().f() + 'D'

    def cm(self):
        return self, super().cm(), 'D'

    @classmethod
    def cm_2(cls):
        return super().cm() + ('D',)


class B(A):
    def f(self):
        return super().f() + 'B'


class C(B, D):
    def f(self):
        return super().f() + 'C'

    def a(self):
        return super().a() + 'C'


def test_super():
    assert D().f() == 'AD'
    assert D.f(D()) == 'AD'

    d = D()
    assert d.cm() == (d, (D, 'A'), 'D')

    assert d.cm_2() == (D, 'A', 'D')
    assert D.cm_2() == (D, 'A', 'D')

    assert C().f() == 'ADBC'
    assert C().a() == 'AC'
