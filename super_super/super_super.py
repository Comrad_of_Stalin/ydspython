import inspect
import copy
import types


class super:
    def __init__(self):
        obj = inspect.currentframe()
        f = obj.f_back
        self_name = list(f.f_locals)[0]

        self.self_obj = f.f_locals[self_name]
        self.self_instance = f.f_locals[self_name].__class__
        # print('CLASS:')
        # print(self.self_instance)
        # if type(self.self_obj) == type:
        #     self.self_instance = self.self_obj

    def __getattr__(self, item):
        if type(self.self_obj) != type:
            to_cls = self.self_instance.__mro__[1]
            tgt_instance = self.self_instance
        else:
            to_cls = self.self_obj.__mro__[1]
            tgt_instance = self.self_obj
        method = getattr(to_cls, item)
        if isinstance(method, classmethod) or isinstance(method, types.MethodType) or tgt_instance == self.self_obj:
            return method.__func__.__get__(tgt_instance, tgt_instance)
        return method.__get__(self.self_obj)

# # super_super = SuperSuper
# def super_super():
#     obj = inspect.currentframe()
#     f = obj.f_back
#     # print(f.f_code.co_filename)
#     # print(f.f_code.co_name)
#     # print(f.f_code.co_varnames)
#     # print(f.f_code.co_nlocals)
#     # print(f.f_locals)
#     # print(f.f_trace)
#     # print(f.f_builtins)
#
#     self_name = list(f.f_locals)[0]
#
#     self_obj = f.f_locals[self_name]
#
#     self_instance = self_obj.__class__
#
#     # mro = self_instance.__mro__
#     #
#     # next(mro)
#     # self_obj_copy = copy.deepcopy(self_obj)
#     tgt_cls = list(self_instance.__mro__)[1]
#
#     # f.f_locals[self_instance] = list(self_instance.__mro__)[1]
#
#     return self_obj


class A:
    # noinspection PyMethodMayBeStatic
    def f(self):
        return 'A'

    @classmethod
    def cm(cls):
        return cls, 'A'

    # noinspection PyMethodMayBeStatic
    def a(self):
        return 'A'


class D(A):
    def f(self):
        return super().f() + 'D'

    def cm(self):
        print(super().cm())
        return self, super().cm(), 'D'

    @classmethod
    def cm_2(cls):
        return super().cm() + ('D',)

    def q(self):
        self.x = 0


class B(A):
    def f(self):
        return super().f() + 'B'


class C(B, D):
    def f(self):
        return super().f() + 'C'

    def a(self):
        return super().a() + 'C'


def test_super():
    assert D().f() == 'AD'
    assert D.f(D()) == 'AD'

    d = D()
    assert d.cm() == (d, (D, 'A'), 'D')

    assert d.cm_2() == (D, 'A', 'D')
    assert D.cm_2() == (D, 'A', 'D')

    assert C().f() == 'ADBC'
    assert C().a() == 'AC'


test_super()
