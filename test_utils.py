import subprocess
import sys


def check_output(script, input_text, expected_output):
    output = subprocess.check_output(
        [sys.executable, script],
        input=input_text,
        encoding='utf-8'
    )
    print(output)
    assert output == expected_output
