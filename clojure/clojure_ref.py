import argparse

import requests


def main(script_filename):
    with open(script_filename) as input_:
        response = requests.post('https://2018.shad-python-minsk.org/api/clojure', data={'script': input_.read()})

    response.raise_for_status()
    print(response.text, end='')


if __name__ == '__main__':
    arguments_parser = argparse.ArgumentParser()
    arguments_parser.add_argument('-c', '--script', required=True, metavar='FILE', help='path to clojure file')

    arguments = arguments_parser.parse_args()

    main(arguments.script)
