import sys
import linecache
import inspect


def exception_info(func):
    try:
        return func()
    except:
        tt, val, tb = sys.exc_info()
        while 1:
            if not tb.tb_next:
                break
            tb = tb.tb_next

        line = linecache.getline(
            tb.tb_frame.f_code.co_filename,
            tb.tb_frame.f_lineno - 1
        )
        arg_list = inspect.getargvalues(tb.tb_frame).args
        local_vars = inspect.getargvalues(tb.tb_frame).locals
        args = {}
        for arg in arg_list:
            args[arg] = local_vars[arg]
        return line, args
