from .exception_info import exception_info


def func_with_exception(a, b):
    a += b
    raise ValueError()


def func_without_exception():
    return 42


def _test_func():
    return func_with_exception(1, 2)


def test_simple():
    line, results = exception_info(_test_func)
    assert line.strip() == 'a += b'
    assert results == {'a': 3, 'b': 2}

    assert exception_info(func_without_exception) == 42
