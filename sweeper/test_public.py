from itertools import zip_longest

from .sweeper import sweep


@sweep
def simple(a, b):
    return a - b


def test_simple():
    for expected, result in zip_longest([((3, 1), 2), ((3, 2), 1), ((4, 1), 3), ((4, 2), 2)], simple([3, 4], [1, 2])):
        expected_arguments, expected_result = expected
        assert expected_arguments == result.arguments
        assert expected_result == result.result
