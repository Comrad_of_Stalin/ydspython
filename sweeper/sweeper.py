import itertools


class SuperStruct:
    def __init__(self, args, val):
        self.arguments = args
        self.result = val


def sweep(func):
    def wrapper(*args):
        arguments = itertools.product(*args)
        return [SuperStruct(tuple(x), func(*x)) for x in arguments]
    return wrapper
