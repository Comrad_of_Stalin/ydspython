from typing import Dict


class TransactionalStorage:
    def __init__(self, data: Dict[str, str]):
        pass

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def __delitem__(self, key):
        pass

    def __setitem__(self, key, value):
        pass
