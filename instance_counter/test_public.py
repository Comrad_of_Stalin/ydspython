import pytest

from .semaphore import InstanceSemaphore


def test_semaphore():
    class A(metaclass=InstanceSemaphore):
        __max_instance_count__ = 1

    _ = A()

    with pytest.raises(TypeError):
        _ = A()
