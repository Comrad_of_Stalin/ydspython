def flatten_it(iterable, hash_storage=None):
    if hash_storage is None:
        hash_storage = []
    if id(iterable) in hash_storage:
        raise ValueError('Ploho tak delyat!')
    hash_storage.append(id(iterable))
    try:
        for item in iterable:
            try:
                for subitem in flatten_it(item, hash_storage):
                    yield subitem
            except ValueError:
                raise
            except TypeError:
                yield item
    except ValueError:
        raise
    except TypeError:
        yield iterable
    hash_storage.pop()
