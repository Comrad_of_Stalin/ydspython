from .dummy_data import initial_db_state


__all__ = ['get_by_id', 'create', 'update_by_id', 'delete_by_id', 'get_all', 'get_by_team']


db_state = initial_db_state


def get_all():
    return db_state


def get_by_id(id_):
    return next((i for i in get_all() if i['id'] == id_), None)


def get_by_team(team):
    return list(filter(lambda i: i['team'] == team, get_all()))


def create(payload):
    data = get_all()
    payload['id'] = max(i['id'] for i in data) + 1 if data else 0
    db_state.append(payload)
    return payload


def update_by_id(id_, update_values):
    item = get_by_id(id_)
    if item is None:
        return None
    item.update({k: v for k, v in update_values.items() if k != 'id'})
    return item


def delete_by_id(id_):
    global db_state
    db_state = [i for i in get_all() if i['id'] != id_]
