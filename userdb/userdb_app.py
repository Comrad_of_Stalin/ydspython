from typing import Tuple, Union

from flask import Response, jsonify, Flask


def create_response(data: Union[dict, list, str] = None, status: int = 200) -> Tuple[Response, int]:
    if data is not None and not isinstance(data, (dict, list, str)):
        raise TypeError('Invalid type of data')

    return jsonify(data), status


app = Flask(__name__)
app.config['WTF_CSRF_ENABLED'] = False


@app.route('/')
def hello_world():
    return create_response({'content': 'hello world!'})


@app.route('/mirror/<name>')
def mirror(name):
    return create_response({'name': name})


if __name__ == '__main__':
    app.run(debug=True)
