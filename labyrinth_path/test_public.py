import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

from test_utils import check_output


def test_example1():
    check_output(
        './labyrinth_path.py',

        # stdin
        '5 3\n'
        '...\n'
        '...\n'
        '..*\n'
        '.#.\n'
        '..#\n',

        # stdout
        '4 3 2\n'
        '3 2 1\n'
        '2 1 0\n'
        '3 -1 1\n'
        '4 5 -1\n'
    )
