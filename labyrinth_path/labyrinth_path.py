from collections import deque


def bfs(mapka, dist, n, m):
    posx, posy = -1, -1
    q = deque()
    visited = [[False for j in range(m)] for i in range(n)]
    for i in range(n):
        for j in range(m):
            if mapka[i][j] == '*':
                posx, posy = i, j
    dist[posx][posy] = 0
    q.append((posx, posy, -1))
    visited[posx][posy] = True
    while len(q) != 0:
        curx, cury, prev_dst = q.popleft()
        dist[curx][cury] = min(dist[curx][cury], prev_dst + 1)
        for i in [curx - 1, curx + 1]:
            if 0 <= i < n and mapka[i][cury] != '#' and not visited[i][cury]:
                q.append((i, cury, dist[curx][cury]))
                visited[i][cury] = True
        for j in [cury - 1, cury + 1]:
            if 0 <= j < m and mapka[curx][j] != '#' and not visited[curx][j]:
                q.append((curx, j, dist[curx][cury]))
                visited[curx][j] = True


if __name__ == '__main__':
    n, m = map(int, input().split())
    dist = [[1e9+7 for j in range(m)] for i in range(n)]
    mapka = []
    for i in range(n):
        line = input().strip()
        mapka.append(line)

    bfs(mapka, dist, n, m)

    for i in range(n):
        for j in range(m):
            if dist[i][j] == 1e9+7:
                dist[i][j] = -1

    for i in range(n):
        print(' '.join([str(x) for x in dist[i]]))
